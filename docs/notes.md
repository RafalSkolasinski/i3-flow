# General

WIP: Just rough notes with design ideas and thoughts...




# CLI Interface

Workspace: left/right - cycle to left/right through workspaces of active group (and current output)

Workspace: assign to a specified group





# Groups

## Mode disabled

- cycle through all workspaces on current output
- if active group on output is `None` it means group mode disabled

## Mode active

- cycle through workspaces should be limited to currently active group

## Default group

- Default group is `0: Default`
- If workspace has no group assigned or group is `None` it belongs to default group

## Activate Next / Prev

Activate next / previous group from currently existing groups.












# Workspace events

Whatever happens it's better it won't create new events so we don't have a chain reaction...

## Init event

- should assign new workspace to currently active group (on this output)

## Focus event

- should activate group of newly focused workspace

## Empty event

- trigger "garbage" collector (so we don't have memory leaks or other bad stuff...)
