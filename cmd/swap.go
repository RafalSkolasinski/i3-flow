package cmd

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"

	pb "gitlab.com/rskolasinski/i3-flow/cmd/proto"
)

var swapCmd = &cobra.Command{
	Use:   "swap [direction]",
	Short: "swap adjacent workspaces",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		direction := args[0]
		fmt.Println(direction)

		moveDirection, err := getDirection(direction)
		if err != nil {
			log.Fatalf("Invalid direction: %v\n", direction)
		}

		swapCommand := pb.SwapCommand{
			Direction: moveDirection,
		}

		grpcSwap(&swapCommand)
	},
}

func grpcSwap(cmd *pb.SwapCommand) {
	fmt.Printf("%v\n", cmd)

	// Set up a connection to the server.
	address := "unix://" + socketFile
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(100*time.Millisecond))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGrpcDaemonClient(conn)

	// Contact the server and print out its response.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.SwapWorkspace(ctx, cmd)

	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Success: %t", r.GetSuccess())
}
