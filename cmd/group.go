package cmd

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"

	pb "gitlab.com/rskolasinski/i3-flow/cmd/proto"
)

func init() {
	f := groupCmd.PersistentFlags()
	f.Bool("move-container", false, "If to move together with active container.")

	rootCmd.AddCommand(groupCmd)
	groupCmd.AddCommand(activateNone)
	// groupCmd.AddCommand(activateGroup)
	groupCmd.AddCommand(sendToGroup)
	groupCmd.AddCommand(activateNext)
	groupCmd.AddCommand(activatePrev)
}

var groupCmd = &cobra.Command{
	Use:   "group [name]",
	Short: "execute group commmand",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		group_name := args[0]
		fmt.Println("Activate group: ", group_name)

		GroupCommand := pb.GroupCommand{
			Command:   pb.COMMAND_ACTIVATE_GROUP,
			GroupName: group_name,
		}

		grpcGroupCmd(&GroupCommand)
	},
}

var activateNext = &cobra.Command{
	Use:   "next",
	Short: "activate next group",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {

		GroupCommand := pb.GroupCommand{
			Command: pb.COMMAND_ACTIVATE_NEXT,
		}

		grpcGroupCmd(&GroupCommand)
	},
}

var activatePrev = &cobra.Command{
	Use:   "prev",
	Short: "activate prev group",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {

		GroupCommand := pb.GroupCommand{
			Command: pb.COMMAND_ACTIVATE_PREV,
		}

		grpcGroupCmd(&GroupCommand)
	},
}

var activateNone = &cobra.Command{
	Use:   "none",
	Short: "disable group mode",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {

		GroupCommand := pb.GroupCommand{
			Command: pb.COMMAND_DISABLE_GROUP_MODE,
		}

		grpcGroupCmd(&GroupCommand)
	},
}

var sendToGroup = &cobra.Command{
	Use:   "send-workspace [name]",
	Short: "assign current workspace to specified group",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		group_name := args[0]
		fmt.Println("Activate group: ", group_name)

		GroupCommand := pb.GroupCommand{
			Command:   pb.COMMAND_ASSIGN_WORKSPACE_TO_GROUP,
			GroupName: group_name,
		}

		grpcGroupCmd(&GroupCommand)
	},
}

// var movePrev = &cobra.Command{
// 	Use:   "prev",
// 	Short: "move to the previous group",
// 	Args:  cobra.ExactArgs(0),
// 	Run: func(cmd *cobra.Command, args []string) {
// 		moveContainer, err := cmd.Flags().GetBool("move-container")
// 		if err != nil {
// 			panic(err)
// 		}
// 		fmt.Println("Move container:", moveContainer)

// 		GroupCommand := pb.GroupCommand{
// 			Command:       pb.COMMAND_ACTIVATE_PREV,
// 			MoveContainer: moveContainer,
// 		}

// 		grpcGroupCmd(&GroupCommand)
// 	},
// }

// var moveNext = &cobra.Command{
// 	Use:   "next",
// 	Short: "move to the next group",
// 	Args:  cobra.ExactArgs(0),
// 	Run: func(cmd *cobra.Command, args []string) {
// 		moveContainer, err := cmd.Flags().GetBool("move-container")
// 		if err != nil {
// 			panic(err)
// 		}
// 		fmt.Println("Move container:", moveContainer)

// 		GroupCommand := pb.GroupCommand{
// 			Command:       pb.COMMAND_ACTIVATE_NEXT,
// 			MoveContainer: moveContainer,
// 		}

// 		grpcGroupCmd(&GroupCommand)
// 	},
// }

func grpcGroupCmd(cmd *pb.GroupCommand) {
	fmt.Printf("%v\n", cmd)

	// Set up a connection to the server.
	address := "unix://" + socketFile
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(100*time.Millisecond))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGrpcDaemonClient(conn)

	// Contact the server and print out its response.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.GroupControl(ctx, cmd)

	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Success: %t", r.GetSuccess())
}
