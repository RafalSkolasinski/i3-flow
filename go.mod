module gitlab.com/rskolasinski/i3-flow

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	google.golang.org/grpc v1.30.0
)
