package main

import (
	"gitlab.com/rskolasinski/i3-flow/cmd"
)

func main() {
	cmd.Execute()
}
