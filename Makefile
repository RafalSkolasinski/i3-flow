GOPATH := $(shell go env GOPATH)


echo:
	echo ${GOPATH}

check: flakes linter

flakes:
	poetry run pyflakes .
	go vet ./...

linter:
	poetry run black .
	go fmt ./...

go-cmd:
	go build -o ${GOPATH}/bin/i3flowctl .

user-install:
	python3 helpers/create_setup.py
	python3 -m pip install -e .
	rm setup.py

.PHONY: protos
protos: go-protos py-protos

py-protos:
	poetry run python -m grpc_tools.protoc \
	    -I ./protos ./protos/i3flow.proto \
	    --python_out ./i3_flow/proto/ --grpc_python_out ./i3_flow/proto/
	sed -i "s/import i3flow_pb2/import i3_flow.proto.i3flow_pb2/g" i3_flow/proto/i3flow_pb2_grpc.py

go-protos:
	protoc -I ./protos/ ./protos/i3flow.proto \
	--go_out=plugins=grpc:cmd/proto --go_opt=paths=source_relative
