from typing import List, Dict
from pydantic import BaseModel

import logging
import i3ipc


logging.basicConfig(level=logging.DEBUG)


class PydanticBase(BaseModel):
    class Config:
        validate_assignment = True


DEFAULT_GROUP = "0: Default"

# ALL_GROUPS = [
#     DEFAULT_GROUP,
#     "1: GROUP",
#     "2: GROUP",
#     "3: GROUP",
#     "4: GROUP",
#     "5: GROUP",
#     "6: GROUP",
#     "7: GROUP",
#     "8: GROUP",
#     "9: GROUP",
# ]


class State(PydanticBase):
    """State representation."""

    # Mapping from output name to group name
    active_groups: Dict[str, str] = {}

    # Mapping from workspace id to group name
    group_assignments: Dict[int, str] = {}

    # mapping from workspace id to visit time
    visit_time: Dict[int, int] = {}

    def get_all_groups(self):
        return sorted(set(self.group_assignments.values()))

    def rename_group(self, old_name, new_name):
        logging.debug(f"Renaming group '{old_name}' -> '{new_name}'")
        self.group_assignments = {
            key: (val if val != old_name else new_name)
            for key, val in self.group_assignments.items()
        }

        self.active_groups = {
            key: (val if val != old_name else new_name)
            for key, val in self.active_groups.items()
        }

    def assign_output_to_group(self, output_name: str, group_name: str):
        """Set Group on specified Output."""
        self.active_groups[output_name] = group_name

    def output_group(self, output_name: str) -> str:
        """Return name of group on output"""
        return self.active_groups.get(output_name, None)

    def assign_workspace_to_group(self, workspace_id: int, group_name: str):
        """"Set Group on specified Workspacee."""
        self.group_assignments[workspace_id] = group_name

    def workspace_group(self, workspace_id: int) -> str:
        """Return group name of workskpace."""
        return self.group_assignments.get(workspace_id, DEFAULT_GROUP)

    def assign_group(self, workspace_id: int, group_name: str):
        """Assign workspace to given group."""
        self.group_assignments[workspace_id] = group_name

    def get_workspaces(self, group_name: str) -> List[str]:
        """Return all workspaces belonging to group."""
        output = []
        for ws_id, ws_group in self.group_assignments.items():
            if ws_group == group_name:
                output.append(ws_id)
        return output


def init_state(i3: i3ipc.Connection) -> State:
    """Set initial state."""
    state = State()

    for ws in i3.get_tree().workspaces():
        state.assign_group(ws.id, DEFAULT_GROUP)

    for output in i3.get_outputs():
        state.assign_output_to_group(output.name, DEFAULT_GROUP)

    logging.debug(f"GROUP STATE: {state}")
    return state


def return_state_info(i3: i3ipc.Connection, state: State) -> Dict:
    pass
